package com.lcg.datafabric.customer.retail.moh;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lcg.datafabric.customer.retail.moh.service.ExtractionService;

@RestController
@Service
public class RESTService {

	@Autowired
	private ExtractionService extractionService;
	
	@GetMapping(name = "extract",path = "extract")
	public void extract() {
		extractionService.startExtraction();
	}
}