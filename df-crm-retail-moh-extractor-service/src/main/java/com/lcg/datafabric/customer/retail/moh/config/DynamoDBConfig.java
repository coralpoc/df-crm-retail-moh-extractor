package com.lcg.datafabric.customer.retail.moh.config;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;


public class DynamoDBConfig {

    public static AmazonDynamoDB createDynamoDBClient() {
        return AmazonDynamoDBClientBuilder.defaultClient();
    }
}
