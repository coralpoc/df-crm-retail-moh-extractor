package com.lcg.datafabric.customer.retail.moh.config;

import java.io.Serializable;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;

public class KafkaReportSender implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/*
     * Bill Pugh's singleton for lazy evaluation
     */
    private static class KafkaReportSenderHolder {

        private static transient KafkaReportSender instance;

        static synchronized KafkaReportSender getInstance(Properties properties) {
            if (instance == null) {
                Properties kafkaProps = new Properties();
                kafkaProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, properties.getProperty("kafka.bootstrapServer"));
                kafkaProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
                kafkaProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
                instance = new KafkaReportSender(new KafkaProducer<>(kafkaProps));
            }
            return instance;
        }

    }

    public static KafkaReportSender getInstance(Properties properties) {
        return KafkaReportSenderHolder.getInstance(properties);
    }

    private final transient KafkaProducer<String, byte[]> producer;

    private KafkaReportSender(KafkaProducer<String, byte[]> producer) {
        this.producer = producer;
    }

    public void sendMessage(String topicName, String key, byte[] message) {
        try {
            producer.send(new ProducerRecord<>(topicName, key, message)).get(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Thread.interrupted();
            throw new IllegalStateException(e);
        } catch (TimeoutException | ExecutionException e) {
            throw new IllegalStateException(e);
        }
    }

}
