package com.lcg.datafabric.customer.retail.moh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.retail.moh.service.DynamoDBStatusService;
import com.lcg.datafabric.customer.retail.moh.service.ExtractionService;

import ch.qos.logback.classic.Level;

@RestController
public class RetailExtractionController {

	@Autowired
	private DataFabricLogger2 dataFabricLogger;

	@Autowired
	private ExtractionService extractionService;

	@Autowired
	private DynamoDBStatusService dynamoDBStatusService;

	@RequestMapping("/startrextraction")
	public synchronized String startExtractionManaully() {
		dataFabricLogger.log(Level.INFO, RetailExtractionController.class,
				"Retail extraction endpoint called, starting extraction manually", Level.INFO.levelStr);
		try {
			if (!dynamoDBStatusService.isRetailViewAlreadyExist()) {
				dataFabricLogger.log(Level.INFO, RetailExtractionController.class,
						"No retail view available in dynamoDB. Hence, starting extraction manually",
						Level.INFO.levelStr);
				extractionService.startExtraction();
			} else {
				dataFabricLogger.log(Level.INFO, RetailExtractionController.class,
						"Retail view available in dynamoDB .Hence, not triggering the service manually",
						Level.INFO.levelStr);
			}

		} catch (Exception ex) {
			dataFabricLogger.log(Level.ERROR, RetailExtractionController.class,
					"Error during extracting retail data manually", Level.ERROR.levelStr, ex);
		}
		return "Retail extraction process started. Check application logs for progress.\n";
	}

	@RequestMapping(value = "/shutdown")
	public synchronized void shutDownService() {
		if(dynamoDBStatusService.isOkToShutdownService()) {
			extractionService.shutDownService();
		}
	}

}
