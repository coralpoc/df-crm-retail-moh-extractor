package com.lcg.datafabric.customer.retail.moh.dto;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class ControlTableResponse {
	
	public LocalDateTime etlDateTime;
	public int recordCount;
	
	public LocalDateTime getEtlDateTime() {
		return etlDateTime;
	}
	public void setEtlDateTime(LocalDateTime etlDateTime) {
		this.etlDateTime = etlDateTime;
	}
	public int getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}

}
