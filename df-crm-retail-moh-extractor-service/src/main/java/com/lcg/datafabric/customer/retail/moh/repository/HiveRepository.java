package com.lcg.datafabric.customer.retail.moh.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.retail.moh.dto.ControlTableResponse;
import com.zaxxer.hikari.HikariDataSource;

import ch.qos.logback.classic.Level;
import lombok.extern.slf4j.Slf4j;

@Repository
@Transactional(readOnly = true)
@Slf4j
public class HiveRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private DataFabricLogger2 dataFabricLogger;

	private final Predicate<List<ControlTableResponse>> validateList = list -> Objects.nonNull(list) && list.size() > 1;

	private final String DATE_PATTERN = "yyyyMM";

	private final String CONTROL_TABLE_QUERY = "select etl_date,record_count from LC_HADOOP_PROCESS_MONITOR where processname = 'gvc_teradata_prd_etl' and project = 'TD_HADOOP_OUT_BATCHES_MOH' and batch_id ='135' and tname='lcg_internal_db.fact_moh_account' and date(etl_date) =  ? and etl_yearmonth = ?" ;

	private final String DATA_QUERY = "SELECT case when a.brand='2' then 'LADBROKES' else 'CORAL' end brand ,a.username, b.gvcaccountname customeridentifier, a.moh_score MOHScore,to_date(from_unixtime(unix_timestamp(cast(a.transactiondateid as varchar(10)) ,'yyyyMMdd'))) MOHScoreDate FROM hive.lcg_internal_db.fact_moh_account a join hive.lcg_internal_db.dim_customer b on a.customerid=b.customerid and a.brand=b.brand where transactiondateid=cast(REPLACE(cast(date_add(current_date,-1) as VARCHAR(10)),'-','') as int)";

	public List<Map<String, Object>> fetchAllRetailData() {

		List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			connection = jdbcTemplate.getDataSource().getConnection();
			ps = connection.prepareStatement(DATA_QUERY);
			rs = ps.executeQuery();

			while (rs.next()) {
				HiveRowMapper dm = new HiveRowMapper();
				Map<String, Object> resultMap = dm.mapRow(rs, 0);
				data.add(resultMap);
			}
		} catch (SQLException e) {
			dataFabricLogger.log(Level.ERROR, HiveRepository.class, "An error occurred while querying hive.", "ERROR",
					e);
		} finally {
			cleanUpResources(connection, ps, rs);
		}
		return data;
	}

	public Optional<ControlTableResponse> queryControlTableForAvailableData(final LocalDate runDate) {

		dataFabricLogger.log(Level.DEBUG, HiveRepository.class, "Control table SQL query - " + CONTROL_TABLE_QUERY,
				"DEBUG");

		List<ControlTableResponse> controlTableList = new ArrayList<ControlTableResponse>();
		Connection connection = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		try {

			connection = jdbcTemplate.getDataSource().getConnection();
			ps = connection.prepareStatement(CONTROL_TABLE_QUERY);
			ps.setString(1, runDate.format(DateTimeFormatter.ISO_LOCAL_DATE));
			ps.setInt(2, Integer.parseInt(DateTimeFormatter.ofPattern(DATE_PATTERN).format(runDate)));

			rs = ps.executeQuery();

			while (rs.next()) {
				ControlTableResponse resp = new ControlTableResponse();
				resp.setEtlDateTime(rs.getTimestamp(1).toLocalDateTime());
				resp.setRecordCount(rs.getInt(2));
				controlTableList.add(resp);
			}

		} catch (Exception ex) {
			dataFabricLogger.log(Level.ERROR, HiveRepository.class, "An error occurred while querying hive.", "ERROR",
					ex);
			return Optional.empty();
		} finally {
			cleanUpResources(connection, ps, rs);
		}

		if (validateList.test(controlTableList)) {
			String msg = String.format("Extracted %s rows for date %s in control table, should be at most one entry",
					controlTableList.size(), runDate);
			dataFabricLogger.log(Level.WARN, HiveRepository.class, msg, "WARN");
		}

		return Objects.nonNull(controlTableList) ? controlTableList.stream().findFirst() : Optional.empty();
	}

	private void cleanUpResources(Connection connection, PreparedStatement ps, ResultSet rs) {

		try {

			if (rs != null) {
				rs.close();
			}

			if (ps != null) {
				ps.close();
			}

			if (connection != null) {
				connection.close();
			}

		} catch (SQLException e) {
			dataFabricLogger.log(Level.ERROR, HiveRepository.class,
					"An error occurred while closing the connection to the hive db.", "ERROR", e);
		}
	}
}
