package com.lcg.datafabric.customer.retail.moh.repository;

import static com.lcg.datafabric.customer.retail.moh.view.dynamodb.ExtractionStatusTableColumns.extractionEndTime;
import static com.lcg.datafabric.customer.retail.moh.view.dynamodb.ExtractionStatusTableColumns.extractionStartTime;
import static com.lcg.datafabric.customer.retail.moh.view.dynamodb.ExtractionStatusTableColumns.streamingEndTime;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.lcg.datafabric.customer.dpl.extractor.dto.ExtractionStatusKey;
import com.lcg.datafabric.customer.retail.moh.view.status.ViewStatus;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Setter
public class RetailDynamoDBHandler  {
	
	private final transient AmazonDynamoDB dynamoDB;
	
	protected String extractionStatusTable;
	
	public RetailDynamoDBHandler(AmazonDynamoDB dynamoDB) {
		 this.dynamoDB = dynamoDB;
	}
	
	public void setStatus(ExtractionStatusKey statusKey, ViewStatus viewStatus) {
        dynamoDB.updateItem(getUpdateItemRequest(statusKey, viewStatus));
    }
	
	public UpdateItemRequest getUpdateItemRequest(ExtractionStatusKey statusKey, ViewStatus viewStatus) {
        AttributeValue currentTimeAttributeValue = buildCurrentTimeAttributeValue();
        log.info("Updating view {} to status {}", statusKey.getTableName(), viewStatus);
        Map<String, AttributeValue> attributeValues = new HashMap<>();
        attributeValues.put(":extractionStatus", new AttributeValue().withS(viewStatus.name()));

        String timeStampColumnName = null;
        String updateExpression = null;
        switch(viewStatus) {
            case EXTRACTING: {
                timeStampColumnName = extractionStartTime.name();
                updateExpression = buildUpdateExpression("extractionStatus", timeStampColumnName);
                attributeValues.put(":" + timeStampColumnName, currentTimeAttributeValue);
                log.info("Setting {} to {}", timeStampColumnName, currentTimeAttributeValue.getS());
                break;
            }
            case EXTRACTED: {
                timeStampColumnName = extractionEndTime.name();
                updateExpression = buildUpdateExpression("extractionStatus", timeStampColumnName);
                attributeValues.put(":" + timeStampColumnName, currentTimeAttributeValue);
                log.info("Setting {} to {}", timeStampColumnName, currentTimeAttributeValue.getS());
                break;
            }
            case SENT_TO_KAFKA: {
                timeStampColumnName = streamingEndTime.name();
                updateExpression = buildUpdateExpression("extractionStatus", timeStampColumnName);
                attributeValues.put(":" + timeStampColumnName, currentTimeAttributeValue);
                log.info("Setting {} to {}", timeStampColumnName, currentTimeAttributeValue.getS());
                break;
            }
            default: {
                updateExpression = "SET extractionStatus = :extractionStatus";
            }
        }

        UpdateItemRequest updateItemRequest = new UpdateItemRequest()
                .withTableName(extractionStatusTable)
                .withKey(
                        new AbstractMap.SimpleEntry("sourceAvailDate", new AttributeValue()
                                .withS(statusKey.getSourceAvailabilityDate().format(DateTimeFormatter.ISO_LOCAL_DATE))),
                        new AbstractMap.SimpleEntry("tableName", new AttributeValue()
                                .withS(statusKey.getTableName()))
                )
                .withUpdateExpression(updateExpression)
                .withExpressionAttributeValues(attributeValues);
        return updateItemRequest;
    }
	
	private String buildUpdateExpression(String... columnNames) {
        return  "SET " + Arrays.stream(columnNames).map(c -> c + " = :" + c).collect(Collectors.joining(", "));
    }
	
	private AttributeValue buildCurrentTimeAttributeValue() {
        Instant currentTime = Instant.now();
        return new AttributeValue().withS(currentTime.atZone(ZoneOffset.UTC).format(DateTimeFormatter.ISO_DATE_TIME));
    }

}
