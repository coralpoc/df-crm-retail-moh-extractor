package com.lcg.datafabric.customer.retail.moh.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.retail.moh.service.DynamoDBStatusService;
import com.lcg.datafabric.customer.retail.moh.service.ExtractionService;

import ch.qos.logback.classic.Level;

@Component
@EnableScheduling
public class RetailMoHExtractorServiceScheduler  {
	
	@Autowired
    private DataFabricLogger2 dataFabricLogger;
	
	@Autowired
	private ExtractionService extractionService;
	
	@Autowired
	private DynamoDBStatusService dynamoDBStatusService;
	
	@Scheduled(cron="${retail.extraction.cron}")
	public void initiateExtractionService() {
		
		dataFabricLogger.log(Level.INFO, RetailMoHExtractorServiceScheduler.class, 
                "Retail MOH Extraction scheduler triggered", Level.INFO.levelStr);
		
		try {
			if(dynamoDBStatusService.getDplExtractionStatus()) {
				dataFabricLogger.log(Level.INFO, RetailMoHExtractorServiceScheduler.class, 
		                "Retail Extraction scheduler triggered", Level.INFO.levelStr);
				extractionService.startExtraction();
			}
		}catch(Exception ex) {
			dataFabricLogger.log(Level.ERROR, RetailMoHExtractorServiceScheduler.class, 
            			"Error during extracting retail data", Level.ERROR.levelStr, ex);
		}
	}
	

	@Scheduled(cron="${retail.shutdown.cron}")
    public void shutDownService() throws Exception {
		if(dynamoDBStatusService.isOkToShutdownService()) {
			extractionService.shutDownService();
		}
    }
}
