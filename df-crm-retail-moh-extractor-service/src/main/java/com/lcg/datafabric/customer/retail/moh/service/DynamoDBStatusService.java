package com.lcg.datafabric.customer.retail.moh.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.dynamodbv2.model.QueryRequest;
import com.amazonaws.services.dynamodbv2.model.Select;
import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.dpl.extractor.dto.ExtractionStatusKey;
import com.lcg.datafabric.customer.retail.moh.config.DynamoDBConfig;
import com.lcg.datafabric.customer.retail.moh.repository.RetailDynamoDBHandler;
import com.lcg.datafabric.customer.retail.moh.schedule.RetailMoHExtractorServiceScheduler;
import com.lcg.datafabric.customer.retail.moh.view.dynamodb.ExtractionStatusTableColumns;
import com.lcg.datafabric.customer.retail.moh.view.status.ViewStatus;

import ch.qos.logback.classic.Level;

@Service
public class DynamoDBStatusService {

	private String retailExtractionStatusTable;
	private String columnValueToExtract;
	private String dplExtractionStatusTableName;
	private String retailViewName;

	private final static Integer DPL_VIEWS_COUNT = new Integer(12);
	private final static LocalTime CUTOFF_TIME = LocalTime.of(11, 0, 0);

	private QueryRequest queryRequestBeforeCutoff;
	private QueryRequest queryRequestAfterCutoff;
	private GetItemRequest getItemRequest;

	private AmazonDynamoDB amazonDynamoDB;

	private RetailDynamoDBHandler dynamoDBHandler;

	private DataFabricLogger2 dataFabricLogger;

	private final DateTimeFormatter dateFormatter = DateTimeFormatter.ISO_LOCAL_DATE;
	private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME.withZone(ZoneOffset.UTC);

	@Autowired
	public DynamoDBStatusService(DataFabricLogger2 dataFabricLogger,
			@Value("${retail.extraction.status.tablename}") String retailExtractionStatusTable,
			@Value("${dpl.extraction.status.table.columnname}") String columnValueToExtract,
			@Value("${dpl.extraction.status.tablename}") String dplExtractionStatusTableName) {
		this.amazonDynamoDB = DynamoDBConfig.createDynamoDBClient();
		this.dataFabricLogger = dataFabricLogger;
		this.retailExtractionStatusTable = retailExtractionStatusTable;
		this.columnValueToExtract = columnValueToExtract;
		this.dplExtractionStatusTableName = dplExtractionStatusTableName;
		this.retailViewName = columnValueToExtract;
		dynamoDBHandler = new RetailDynamoDBHandler(amazonDynamoDB);
		dynamoDBHandler.setExtractionStatusTable(retailExtractionStatusTable);
	}

	public void setAmazonDynamoDB(AmazonDynamoDB amazonDynamoDB, String retailExtractionStatusTable) {
		this.amazonDynamoDB = amazonDynamoDB;
		dynamoDBHandler = new RetailDynamoDBHandler(amazonDynamoDB);
		dynamoDBHandler.setExtractionStatusTable(retailExtractionStatusTable);
	}

	private void initDBQuery() {
		queryRequestBeforeCutoff = new QueryRequest(dplExtractionStatusTableName).withSelect(Select.COUNT)
				.withKeyConditionExpression("#srcAvailDate=:srcDate").withFilterExpression("#status=:extractStatus")
				.withExpressionAttributeNames(getAttributeNames())
				.withExpressionAttributeValues(getAttributeValuesBeforeCutoff());

		queryRequestAfterCutoff = new QueryRequest(dplExtractionStatusTableName).withSelect(Select.COUNT)
				.withKeyConditionExpression("#srcAvailDate=:srcDate")
				.withFilterExpression("#status IN (:status1, :status2)")
				.withExpressionAttributeNames(getAttributeNames())
				.withExpressionAttributeValues(getAttributeValuesafterCutoff());

		getItemRequest = new GetItemRequest(retailExtractionStatusTable, getKeyToExtract());
	}
	
	public boolean isDigitalOverDeadline() {
		return LocalTime.now().isAfter(CUTOFF_TIME);
	}

	public boolean getDplExtractionStatus() {
		dataFabricLogger.log(Level.INFO, DynamoDBStatusService.class, "Checking DPL views extraction status",
				Level.INFO.levelStr);
		Integer tableCount = 0;
		boolean status = false;
		try {
			if (!isRetailViewAlreadyExist()) {
				dataFabricLogger.log(Level.DEBUG, DynamoDBStatusService.class,
						"Found no Retail View for today in DynamoDB", Level.DEBUG.levelStr);
				if (isDigitalOverDeadline()) {
					dataFabricLogger.log(Level.DEBUG, DynamoDBStatusService.class,
							"Passed digital deadline, checking DPL status...", Level.DEBUG.levelStr);
					tableCount = amazonDynamoDB.query(queryRequestAfterCutoff).getCount();
					status = tableCount.equals(new Integer(0));
				} else {
					dataFabricLogger.log(Level.DEBUG, DynamoDBStatusService.class,
							"Not passed digital deadline yet, checking DPL status...", Level.DEBUG.levelStr);

					tableCount = amazonDynamoDB.query(queryRequestBeforeCutoff).getCount();
					status = tableCount.equals(DPL_VIEWS_COUNT);
				}
			} else {
				dataFabricLogger.log(Level.DEBUG, DynamoDBStatusService.class,
						"Found Retail View in DynamoDB for today", Level.DEBUG.levelStr);
			}
			dataFabricLogger.log(Level.DEBUG, DynamoDBStatusService.class,
					"DPL views count that have status sent-to-kafka " + tableCount, Level.DEBUG.levelStr);
		} catch (Exception ex) {
			dataFabricLogger.log(Level.ERROR, RetailMoHExtractorServiceScheduler.class,
					"Error finding the count of DPL views that are sent to kafka", Level.ERROR.levelStr, ex);
		}
		return status;
	}

	private Map<String, String> getAttributeNames() {
		Map<String, String> expressionAttributeNames = new HashMap<>();
		expressionAttributeNames.put("#srcAvailDate", ExtractionStatusTableColumns.sourceAvailDate.name());
		expressionAttributeNames.put("#status", ExtractionStatusTableColumns.extractionStatus.name());
		return expressionAttributeNames;
	}

	private Map<String, AttributeValue> getAttributeValuesBeforeCutoff() {
		Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();
		expressionAttributeValues.put(":srcDate", new AttributeValue().withS(LocalDate.now().format(dateFormatter)));
		expressionAttributeValues.put(":extractStatus", new AttributeValue().withS(ViewStatus.SENT_TO_KAFKA.name()));
		return expressionAttributeValues;
	}

	private Map<String, AttributeValue> getAttributeValuesafterCutoff() {
		Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();
		expressionAttributeValues.put(":srcDate", new AttributeValue().withS(LocalDate.now().format(dateFormatter)));
		expressionAttributeValues.put(":status1", new AttributeValue().withS(ViewStatus.READY_TO_JOIN.name()));
		expressionAttributeValues.put(":status2", new AttributeValue().withS(ViewStatus.JOINING.name()));
		return expressionAttributeValues;
	}

	private Map<String, AttributeValue> getKeyToExtract() {
		Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();
		expressionAttributeValues.put(ExtractionStatusTableColumns.sourceAvailDate.name(),
				new AttributeValue().withS(LocalDate.now().format(dateFormatter)));
		expressionAttributeValues.put(ExtractionStatusTableColumns.tableName.name(),
				new AttributeValue().withS(columnValueToExtract));
		return expressionAttributeValues;
	}

	private Map<String, AttributeValue> getKeyToExtract(LocalDate runDate) {
		Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();
		expressionAttributeValues.put(ExtractionStatusTableColumns.sourceAvailDate.name(),
				new AttributeValue().withS(runDate.format(DateTimeFormatter.ISO_LOCAL_DATE)));
		expressionAttributeValues.put(ExtractionStatusTableColumns.tableName.name(),
				new AttributeValue().withS(retailViewName));
		return expressionAttributeValues;
	}

	public boolean isRetailViewAlreadyExist() {
		initDBQuery();
		boolean viewExist = false;
		try {
			if(this.getRetailViewForDate() != null) {
				viewExist = true;
			}
			dataFabricLogger.log(Level.DEBUG, DynamoDBStatusService.class,
					"Check for retail view already exist returned - " + viewExist, Level.DEBUG.levelStr);
		} catch (Exception ex) {
			dataFabricLogger.log(Level.ERROR, RetailMoHExtractorServiceScheduler.class,
					"Error while checking the status of retail extraction process", Level.ERROR.levelStr, ex);
		}
		return viewExist;
	}

	public void creatRetailEntryForDate(LocalDateTime runDate, LocalDateTime etlDateTime, String batchNo) {
		
		dataFabricLogger.log(Level.INFO, DynamoDBStatusService.class, "Creating a new entry in dynamoDB for retail", "INFO" );

		Map<String, AttributeValue> itemMap = new HashMap<String, AttributeValue>();

		itemMap.put(ExtractionStatusTableColumns.tableName.name(), new AttributeValue(retailViewName));
		itemMap.put(ExtractionStatusTableColumns.sourceAvailDate.name(),
				new AttributeValue(runDate.format(dateFormatter)));
		itemMap.put(ExtractionStatusTableColumns.extractionStatus.name(),
				new AttributeValue(ViewStatus.AVAILABLE.name()));
		itemMap.put(ExtractionStatusTableColumns.sourceAvailTime.name(),
				new AttributeValue(etlDateTime.format(dateTimeFormatter)));
		itemMap.put(ExtractionStatusTableColumns.batchNumber.name(), new AttributeValue(batchNo));

		amazonDynamoDB.putItem(retailExtractionStatusTable, itemMap);

	}

	public Map<String, AttributeValue> getRetailViewForDate() {
		try {
			GetItemResult result = amazonDynamoDB.getItem(getItemRequest);
			if (null != result && null != result.getItem()) {
				return result.getItem();
			}
		} catch (AmazonServiceException ex) {
			dataFabricLogger.log(Level.ERROR, RetailMoHExtractorServiceScheduler.class,
					"Error while checking the status of retail extraction process", Level.ERROR.levelStr, ex);
		}
		return null;
	}
	
	public Map<String, AttributeValue> getRetailViewForDate(LocalDate localDate) {
		try {
			GetItemResult result = amazonDynamoDB.getItem(retailExtractionStatusTable, getKeyToExtract(localDate));

			if (null != result && null != result.getItem()) {
				return result.getItem();
			}

		} catch (AmazonServiceException ex) {
			dataFabricLogger.log(Level.ERROR, RetailMoHExtractorServiceScheduler.class,
					"Error while checking the status of retail extraction process", Level.ERROR.levelStr, ex);
		}
		return null;
	}

	public void updateRetailRunStatus(LocalDate sourceDate, ViewStatus viewStatus) {

		ExtractionStatusKey statusKey = new ExtractionStatusKey();
		statusKey.setSourceAvailabilityDate(sourceDate);
		statusKey.setTableName(retailViewName);

		dynamoDBHandler.setStatus(statusKey, viewStatus);
	}

	public boolean isOkToShutdownService() {
		dataFabricLogger.log(Level.DEBUG, DynamoDBStatusService.class, "Checking is Ok to shutdown service",
				Level.DEBUG.levelStr);
		Map<String, AttributeValue> map = this.getRetailViewForDate(LocalDate.now());
		if (map == null) {
			dataFabricLogger.log(Level.DEBUG, DynamoDBStatusService.class,
					"No dynamo entry available with todays date hence, shutting down the service", Level.DEBUG.levelStr);
			return true;
		} else {
			dataFabricLogger.log(Level.DEBUG, DynamoDBStatusService.class,
					"An entry with status [" + map.get(ExtractionStatusTableColumns.extractionStatus.name()).getS()
							+ "] is available in DynanmoDB hence, not shutting down the service",
					Level.DEBUG.levelStr);
			return false;
		}
	}
}