package com.lcg.datafabric.customer.retail.moh.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lc.df.logging.DataFabricLogger2;
import com.lcg.customer.cdm.field.StringField;
import com.lcg.customer.cdm.model.CustomerRecord;
import com.lcg.customer.common.event.CustomerStreamerEventType;
import com.lcg.datafabric.utils.KafkaSender;
import com.lcg.transform.serialization.CustomerStreamerObjectMapperFactory;
import com.lcg.transform.service.CustomerStreamerRecordProcessor;
import com.lcg.transform.service.FlowType;
import com.newrelic.api.agent.NewRelic;
import com.newrelic.api.agent.Trace;

import ch.qos.logback.classic.Level;

@Component
public class StreamerService {
	
	@Value("${kafka.target-topic}")
	private String targetTopic;

	@Value("${kafka.temp-topic}")
	private String tempTopic;

	@Value("${kafka.partition.size}")
	int kafkaNoPartitions;

	private KafkaSender kafkaSender;
	
	@Autowired
	private DataFabricLogger2 logger;
	
	@Autowired
	private ELKReportService eLKReportService;
	
	private CustomerStreamerRecordProcessor recordProcessor;
	
	public final static String KEY_BOOTSTRAP_SERVER = "kafka.bootstrapServer";
	
	private final ObjectMapper objectMapper;
	
	private int sentToKafkaCount;
	
	public StreamerService(@Value("${kafka.bootstrapServer}") String kafkaBootstrapServer) {
		Properties properties = getConfigProperty(kafkaBootstrapServer);
	
		kafkaSender = KafkaSender.getInstance(properties);
		recordProcessor = CustomerStreamerRecordProcessor.getInstance(properties, kafkaSender);
		objectMapper = CustomerStreamerObjectMapperFactory.createForSerialisation();

	}
	
	
	public Properties getConfigProperty(String kafkaBootstrapServer) {
		
		Properties properties = new Properties();
		properties.put(KEY_BOOTSTRAP_SERVER, kafkaBootstrapServer);
		properties.put(CustomerStreamerRecordProcessor.PROPS_IS_MARKETING_FIELD_LOGIC_REQUESTED, false);
		properties.put(CustomerStreamerRecordProcessor.PROPS_KEY_DER_ENRICHMENT_ENABLED, false);
		properties.put(CustomerStreamerRecordProcessor.PROPS_KEY_FLOW_TYPE, FlowType.RETAIL);
		
		return properties;
	}

	public void sendToTempKafkaTopic(String retailBatchNo, int fetchBatchNo, List<Map<String,Object>> extracts) {
		sentToKafkaCount = 0;
		int processingCount = 1;
		int size = extracts.size();
		String messageKey = "0";
		for(Map<String,Object> recordMap: extracts) {
			String infoMsg = String.format("[Fetch batch no - %d] - Streaming [%d] of [%d] records to temp kafka topic...", fetchBatchNo, processingCount, size);
			logger.log(Level.INFO, StreamerService.class, infoMsg, "INFO");

			try {
				messageKey = calculateKafkaMsgKey(messageKey);
				byte[] messageBody = objectMapper.writeValueAsBytes(recordMap);
				kafkaSender.sendMessage(tempTopic, messageKey, messageBody);
				processingCount++;
			} catch (JsonProcessingException e) {
				throw new IllegalStateException(e);
			}

			sentToKafkaCount++;
		}

		String msg = String.format("%s - Completed streaming [%d] records out of [%d] from HIVE batch no [%d]", retailBatchNo, sentToKafkaCount, size, fetchBatchNo);
		logger.log(Level.INFO, StreamerService.class, msg, "INFO");
	}

	private String calculateKafkaMsgKey(String key) {
		if (StringUtils.isBlank(key) ||
				String.valueOf(kafkaNoPartitions).equalsIgnoreCase(key)) {
			return "1";
		}
		return String.valueOf(Integer.parseInt(key) + 1);
	}
	
	public void processExtracts(String retailBatchNo, int fetchBatchNo, List<Map<String,Object>> extracts) {
		sentToKafkaCount = 0;
		int processingCount = 0;
		int size = extracts.size();
		
		for(Map<String,Object> recordMap: extracts) {
			processingCount++;
			String infoMsg = String.format("[Fetch batch no - %d] - Streaming [%d] of [%d] records ...", fetchBatchNo, processingCount, size);
			logger.log(Level.INFO, StreamerService.class, infoMsg, "INFO");
			streamEachRecord(retailBatchNo, recordMap);
		}
		String msg = String.format("%s - Completed streaming [%d] records out of [%d] from HIVE batch no [%d]", retailBatchNo, sentToKafkaCount, size, fetchBatchNo);
		logger.log(Level.INFO, StreamerService.class, msg, "INFO");
		eLKReportService.sendExtractorInfoToELK(ELKReportService.RETAIL_MOH_RECORDS_SENT_TO_KAFKA, retailBatchNo,
				sentToKafkaCount, "Retail counts sent to kafka");
	}
	
	
	@Trace(dispatcher=true)
	public void streamEachRecord(String retailBatchNo, Map<String,Object> recordMap) {
		try {
			
			if(recordMap.get("mohscore") == null) {
				recordMap.put("mohscore", 0);
				NewRelic.noticeError("MoHScore is null for the record", recordMap);
			}
			
			if(recordMap.get("mohscoredate") == null) {
				NewRelic.noticeError("MoHScoreDate is null for the record", recordMap);
			}
			
			NewRelic.addCustomParameter("TransactionDateId",(String) recordMap.get("transactiondateid"));
			
			CustomerRecord customerRecord = recordProcessor.processRecord(recordMap);
			setFlowBatchNo(retailBatchNo, customerRecord);
			logger.log(Level.INFO, StreamerService.class, "streaming record for customerId - " + 
					customerRecord.getPunter().getAccount().getCustomerIdentifier().getValue() +"===="+ customerRecord, "INFO");
			
			sendMessageToKafka(customerRecord);
			
			NewRelic.addCustomParameter("SentToKafka",(String) recordMap.get("transactiondateid"));
			NewRelic.incrementCounter("sentToKafkaRecords");
	
			logger.log(Level.INFO, StreamerService.class, "Completed streaming record for customerId - " + 
					customerRecord.getPunter().getAccount().getCustomerIdentifier().getValue(), "INFO");
		}catch(Exception e) {
			logger.log(Level.ERROR, StreamerService.class, "Unexpected error while streaming record - " + recordMap, "ERROR",e);
		}
	}
	
	public void setFlowBatchNo(String batchNo,CustomerRecord customerRecord) {
		customerRecord.getRecordMetadata().getBatchMetadata().setLastUpdatedByBatch(new StringField(batchNo));
	}
	
	
	public void sendMessageToKafka(CustomerRecord customerRec) {
		
        try {
            
        	Map<String, Object> wrappedRecord = wrapWithMetadata(customerRec);
        	String customerIdentifier = customerRec.getPunter().getAccount().getCustomerIdentifier().getValue();
            byte[] messageBody = objectMapper.writeValueAsBytes(wrappedRecord);
            
            String msg = String.format("Sending to Kafka customer - %s -> %s", customerIdentifier,
            		customerRec);
            
            logger.log(Level.DEBUG, StreamerService.class, msg, "DEBUG");
            
            kafkaSender.sendMessage(getTargetTopic(), customerIdentifier, messageBody);
            
            msg = String.format("Sent to Kafka customerIdentifier - %s", customerIdentifier);
            
            sentToKafkaCount++;
            
            logger.log(Level.DEBUG, StreamerService.class, msg, "DEBUG");
        } catch (JsonProcessingException e) {
            throw new IllegalStateException(e);
        }
    }
	
	public String getTargetTopic() {
		return targetTopic;
	}
	
	private Map<String, Object> wrapWithMetadata(final CustomerRecord customerRecord) {
        return new HashMap<String, Object>() {{
            put("metadata", new HashMap<String, Object>() {{
                put("eventType", new HashMap<String, String>() {{
                    put("name", CustomerStreamerEventType.CUSTOMER_STREAMER_EVENT_TYPE_NAME);
                }});
                put("eventSource", new HashMap<String, String>() {{
                    put("name", "RETAIL");
                }});
            }});
            put("payload", customerRecord);
        }};
    }

}
