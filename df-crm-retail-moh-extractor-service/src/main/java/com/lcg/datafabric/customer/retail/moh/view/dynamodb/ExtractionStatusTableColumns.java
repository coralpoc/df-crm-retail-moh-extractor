package com.lcg.datafabric.customer.retail.moh.view.dynamodb;

/**
 * These constants are deliberately camel case to match existing column names on all environments.
 */
public enum ExtractionStatusTableColumns {

    sourceAvailDate,
    tableName,
    batchNumber,

    extractionRequestTime,
    extractionStartTime,
    extractionEndTime,
    extractionStatus,

    joiningRequestTime,
    joiningStartTime,
    joiningEndTime,

    s3Path,

    sourceAvailTime,
    timeWindowStart,
    timeWindowEnd,
    
    streamingEndTime
}
