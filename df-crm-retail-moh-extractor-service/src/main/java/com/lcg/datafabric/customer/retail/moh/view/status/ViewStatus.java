package com.lcg.datafabric.customer.retail.moh.view.status;

public enum ViewStatus {
    AVAILABLE,
    EXTRACTING,
    EXTRACTED,
    EXTRACTION_FAILED,
    READY_TO_JOIN,
    JOINING,
    JOIN_FAILED,
    SENT_TO_KAFKA
}
