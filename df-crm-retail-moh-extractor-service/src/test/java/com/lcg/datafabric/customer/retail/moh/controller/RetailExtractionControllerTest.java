package com.lcg.datafabric.customer.retail.moh.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.retail.moh.controller.RetailExtractionController;
import com.lcg.datafabric.customer.retail.moh.service.ExtractionService;

@RunWith(MockitoJUnitRunner.class)
public class RetailExtractionControllerTest {
	
	@Mock
	private ExtractionService extractionService;
	
	@Mock
	private DataFabricLogger2 dataFabricLogger;
	
	@InjectMocks
	private RetailExtractionController controller;
	
	
	@Test
	public void startRetailExtractionService() {
		assertThat(controller.startExtractionManaully(), containsString("Retail extraction process started"));
	}
	

}
