package com.lcg.datafabric.customer.retail.moh.extraction.status;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.testcontainers.containers.GenericContainer;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.retail.moh.RawDBHelperData;
import com.lcg.datafabric.customer.retail.moh.service.BatchNumberHelper;
import com.lcg.datafabric.customer.retail.moh.service.DynamoDBStatusService;
import com.lcg.datafabric.customer.retail.moh.view.dynamodb.ExtractionStatusTableColumns;
import com.lcg.datafabric.customer.retail.moh.view.status.ViewStatus;

import lombok.extern.slf4j.Slf4j;

@Ignore
@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class DplExtractionStatusTest {
	
	private final static String RETAIL_VIEW_NAME = "SFMC_RETAIL";
	private final static String DPL_EXTRACTION_STATUS_TABLE = "dpl-extraction-status";
	private final static String RETAIL_EXTRACTION_STATUS_TABLE = "retail-extraction-status";
	
	 @ClassRule
	 public static GenericContainer dynamoDb = new GenericContainer("dwmkerr/dynamodb:latest")
	            .withExposedPorts(8000);
	 
	 private static AmazonDynamoDB dynamoDbClient;
	 
	 private DynamoDBStatusService service;
	 
	 @Mock
	 private DataFabricLogger2 dataFabricLogger;
	 
	 @BeforeClass
	 public static void initializeLocalDynamoDB() {
	      dynamoDbClient = DynamoDBTestUtils.buildLocalDynamoDBClient(
	                						dynamoDb.getContainerIpAddress(),
	                						dynamoDb.getMappedPort(8000));
	      
	      
	 }
	 
	 @AfterClass
	 public static void shutdownLocalDynamoDB() {
	      dynamoDbClient.shutdown();
	 }
	 
	 @Before
	 public void init() {
		 try {
			 dynamoDbClient.deleteTable(RETAIL_EXTRACTION_STATUS_TABLE);
			 dynamoDbClient.deleteTable(DPL_EXTRACTION_STATUS_TABLE);
		 }catch(ResourceNotFoundException re) {
			 log.info("Cannot delete because Table does not yet exist");
		 }
		 DplExtractorTableCreator.createRetailExtractionTable(dynamoDbClient);
		 DplExtractorTableCreator.createDplExtractionTable(dynamoDbClient);
		 DynamoDBTestUtils.waitForDynamoDbToBeReady(dynamoDbClient, 20000);
		 service = new DynamoDBStatusService( dataFabricLogger, RETAIL_EXTRACTION_STATUS_TABLE, RETAIL_VIEW_NAME, DPL_EXTRACTION_STATUS_TABLE);
		 service.setAmazonDynamoDB(dynamoDbClient, RETAIL_EXTRACTION_STATUS_TABLE);
	 }
	 
	 
	 @Test
	 public void whenNoDplRunAndBeforeDigitalDeadlineThenFalse() {
		 
		 DynamoDBStatusService dbStatusServiceSpy = Mockito.spy(service);
		 when(dbStatusServiceSpy.isDigitalOverDeadline()).thenReturn(false);
		 
		 assertFalse(dbStatusServiceSpy.getDplExtractionStatus());
		 
	 }
	 
	 @Test
	 public void whenNoDplRunAndAfterDigitalDeadlineThenTrue() {
		 
		 DynamoDBStatusService dbStatusServiceSpy = Mockito.spy(service);
		 when(dbStatusServiceSpy.isDigitalOverDeadline()).thenReturn(true);
		 
		 assertTrue(dbStatusServiceSpy.getDplExtractionStatus());
		 
	 }
	 
	 @Test
	 public void whenAllDplRunCompleteThenReturnTrue(){
		 
		 DplExtractorTableInsertion.insertExtractionTable(dynamoDbClient);
		 
		 assertTrue(service.getDplExtractionStatus());
	 }
	 
	@Test
	public void whenBatchRunInProgressAndBeforeDigitalDeadlineThenFalse() {
		
		DplExtractorTableInsertion.simulateBatch0InProgress(dynamoDbClient);
		DynamoDBStatusService dbStatusServiceSpy = Mockito.spy(service);
		when(dbStatusServiceSpy.isDigitalOverDeadline()).thenReturn(false);
		 
		assertFalse(dbStatusServiceSpy.getDplExtractionStatus());
	}
	
	@Test
	public void whenBatchRunInProgressAndAfterDigitalDeadlineThenTrue() {
		
		DplExtractorTableInsertion.simulateBatch0InProgress(dynamoDbClient);
		DynamoDBStatusService dbStatusServiceSpy = Mockito.spy(service);
		when(dbStatusServiceSpy.isDigitalOverDeadline()).thenReturn(true);
		 
		assertTrue(dbStatusServiceSpy.getDplExtractionStatus());
	}
	
	@Test
	public void whenBatchRunCompleteAndBeforeDigitalDeadlineThenFalse() {
		
		DplExtractorTableInsertion.simulateBatch0Complete(dynamoDbClient);
		DynamoDBStatusService dbStatusServiceSpy = Mockito.spy(service);
		when(dbStatusServiceSpy.isDigitalOverDeadline()).thenReturn(false);
		 
		assertFalse(dbStatusServiceSpy.getDplExtractionStatus());
	}
	
	@Test
	public void whenBatchRunCompleteAndBeforeDigitalDeadlineThenTrue() {
		
		DplExtractorTableInsertion.simulateBatch0Complete(dynamoDbClient);
		DynamoDBStatusService dbStatusServiceSpy = Mockito.spy(service);
		when(dbStatusServiceSpy.isDigitalOverDeadline()).thenReturn(true);
		 
		assertTrue(dbStatusServiceSpy.getDplExtractionStatus());
	}
	
	@Test
	public void whenBatchRunJoiningAndBeforeDigitalDeadlineThenFalse() {
		
		DplExtractorTableInsertion.simulateBatchJoining(dynamoDbClient);
		DynamoDBStatusService dbStatusServiceSpy = Mockito.spy(service);
		when(dbStatusServiceSpy.isDigitalOverDeadline()).thenReturn(false);
		 
		assertFalse(dbStatusServiceSpy.getDplExtractionStatus());
	}
	
	@Test
	public void whenBatch0CompleteAndBatc1StartingAndBeforeDigitalDeadlineThenFalse() {
		
		DplExtractorTableInsertion.simulateBatch0CompleteBatch1Starting(dynamoDbClient);
		DynamoDBStatusService dbStatusServiceSpy = Mockito.spy(service);
		when(dbStatusServiceSpy.isDigitalOverDeadline()).thenReturn(false);
		 
		assertFalse(dbStatusServiceSpy.getDplExtractionStatus());
	}
	
	@Test
	public void whenBatchRunJoiningAndAfterDigitalDeadlineThenFalse() {
		
		DplExtractorTableInsertion.simulateBatchJoining(dynamoDbClient);
		DynamoDBStatusService dbStatusServiceSpy = Mockito.spy(service);
		when(dbStatusServiceSpy.isDigitalOverDeadline()).thenReturn(true);
		 
		assertFalse(dbStatusServiceSpy.getDplExtractionStatus());
	}
	
	 @Test
	 public void testRetailViewAvailableStateCreatedCorrectly() {
		 
		 LocalDateTime localDate = LocalDateTime.of(2018, Month.APRIL, 1,0,0);
		 String batchNo = BatchNumberHelper.getBatchNumber(localDate);
		 service.creatRetailEntryForDate(localDate,localDate,batchNo);
		 
		 String sourceAvailDate = localDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
		 
		 Map<String,AttributeValue> itemMap = service.getRetailViewForDate(LocalDate.of(2018, Month.APRIL, 1));
		 
		 
		 assertCorrectStateForAvailableStatus(itemMap,sourceAvailDate,batchNo);
			 
	 }
	 
	 private void assertCorrectStateForAvailableStatus(Map<String,AttributeValue> itemMap,String sourceAvailDate,String batchNo) {
		 assertEquals(RETAIL_VIEW_NAME, 
				 itemMap.get(ExtractionStatusTableColumns.tableName.name()).getS()); 
		 
		 assertEquals(ViewStatus.AVAILABLE.name(),itemMap.get(ExtractionStatusTableColumns.extractionStatus.name()).getS());
		 
		 assertEquals(sourceAvailDate,itemMap.get(ExtractionStatusTableColumns.sourceAvailDate.name()).getS());
		 
		 assertEquals(batchNo,itemMap.get(ExtractionStatusTableColumns.batchNumber.name()).getS());
	 }
	 
	 @Test
	 public void whenRetailRunningAndDPLStatusCheckThenReturnFalse() {
		 
		 LocalDateTime localDate = LocalDateTime.now();
		 service.creatRetailEntryForDate(localDate,localDate,BatchNumberHelper.getBatchNumber(localDate));
		 
		 assertFalse(service.getDplExtractionStatus());
	 }
	 
	 @Test
	 public void testDynamoDbStatusDuringRetailFlowRun() {
		 
		 LocalDateTime localDate = LocalDateTime.now();
		 service.creatRetailEntryForDate(localDate,localDate,BatchNumberHelper.getBatchNumber(localDate));
		 
		 Map<String,AttributeValue> itemMap = service.getRetailViewForDate(LocalDate.now());
		 
		 verifyRunFlowAvailableState(itemMap);
		 
		 service.updateRetailRunStatus(LocalDate.now(), ViewStatus.EXTRACTING);
		 
		 itemMap = service.getRetailViewForDate(LocalDate.now());
		 
		 verifyRunFlowExtractingState(itemMap);
		 
		 service.updateRetailRunStatus(LocalDate.now(), ViewStatus.EXTRACTED);
		 itemMap = service.getRetailViewForDate(LocalDate.now());
		 
		 verifyRunFlowExtractedState(itemMap);
		 
		 service.updateRetailRunStatus(LocalDate.now(), ViewStatus.SENT_TO_KAFKA);
		 itemMap = service.getRetailViewForDate(LocalDate.now());
		 
		 verifyRunFlowSentToKafkaState(itemMap);
		 
		 
	 }
	 
	 private void verifyRunFlowAvailableState(Map<String,AttributeValue> itemMap) {
		 
		 assertEquals(RETAIL_VIEW_NAME,itemMap.get(ExtractionStatusTableColumns.tableName.name()).getS());
		 
		 assertEquals(ViewStatus.AVAILABLE.name(),itemMap.get(ExtractionStatusTableColumns.extractionStatus.name()).getS());
		 
		 String sourceAvailDate = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
		 
		 assertEquals(sourceAvailDate,itemMap.get(ExtractionStatusTableColumns.sourceAvailDate.name()).getS());
		 
		 String batchNo = RawDBHelperData.generateBatchNo();
		 
		 assertTrue(itemMap.get(ExtractionStatusTableColumns.batchNumber.name()).getS().startsWith(batchNo));
	 }
	 
	 private void verifyRunFlowExtractingState(Map<String,AttributeValue> itemMap) {
		 
		 assertEquals(RETAIL_VIEW_NAME,itemMap.get(ExtractionStatusTableColumns.tableName.name()).getS());
		 
		 assertEquals(ViewStatus.EXTRACTING.name(),itemMap.get(ExtractionStatusTableColumns.extractionStatus.name()).getS());
		 
		 assertNotNull(itemMap.get(ExtractionStatusTableColumns.extractionStartTime.name()));
		 
		 assertNull(itemMap.get(ExtractionStatusTableColumns.extractionEndTime.name()));
		 
		 assertNull(itemMap.get(ExtractionStatusTableColumns.streamingEndTime.name()));
	 }
	 
	 private void verifyRunFlowExtractedState(Map<String,AttributeValue> itemMap) {
		 
		 assertEquals(RETAIL_VIEW_NAME,itemMap.get(ExtractionStatusTableColumns.tableName.name()).getS());
		 
		 assertEquals(ViewStatus.EXTRACTED.name(),itemMap.get(ExtractionStatusTableColumns.extractionStatus.name()).getS());
		 
		 assertNotNull(itemMap.get(ExtractionStatusTableColumns.extractionStartTime.name()));
		 
		 assertNotNull(itemMap.get(ExtractionStatusTableColumns.extractionEndTime.name()));
		 
		 assertNull(itemMap.get(ExtractionStatusTableColumns.streamingEndTime.name()));
		 

	 }
	 
	 private void verifyRunFlowSentToKafkaState(Map<String,AttributeValue> itemMap) {
		 
		 assertEquals(RETAIL_VIEW_NAME,itemMap.get(ExtractionStatusTableColumns.tableName.name()).getS());
		 
		 assertEquals(ViewStatus.SENT_TO_KAFKA.name(),itemMap.get(ExtractionStatusTableColumns.extractionStatus.name()).getS());
		 
		 assertNotNull(itemMap.get(ExtractionStatusTableColumns.extractionStartTime.name()));
		 
		 assertNotNull(itemMap.get(ExtractionStatusTableColumns.extractionEndTime.name()));
		 
		 assertNotNull(itemMap.get(ExtractionStatusTableColumns.streamingEndTime.name()));
		 

	 }
	 
	 
	 

	 
}