package com.lcg.datafabric.customer.retail.moh.extraction.status;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.lcg.datafabric.customer.retail.moh.view.dynamodb.ExtractionStatusTableColumns;


public class DplExtractorTableCreator {
	
	private final static String DPL_EXTRACTION_STATUS_TABLE = "dpl-extraction-status";
	private final static String RETAIL_EXTRACTION_STATUS_TABLE = "retail-extraction-status";

    private DplExtractorTableCreator() {
    }

    public static void createRetailExtractionTable(AmazonDynamoDB dynamoDB) {
        dynamoDB.createTable(new CreateTableRequest()
                .withTableName(RETAIL_EXTRACTION_STATUS_TABLE)
                .withKeySchema(
                        new KeySchemaElement().withKeyType(KeyType.HASH).withAttributeName(ExtractionStatusTableColumns.sourceAvailDate.name()),
                        new KeySchemaElement().withKeyType(KeyType.RANGE).withAttributeName(ExtractionStatusTableColumns.tableName.name())
                )
                .withAttributeDefinitions(
                        new AttributeDefinition()
                                .withAttributeName(ExtractionStatusTableColumns.sourceAvailDate.name())
                                .withAttributeType(ScalarAttributeType.S),
                        new AttributeDefinition()
                                .withAttributeName(ExtractionStatusTableColumns.tableName.name())
                                .withAttributeType(ScalarAttributeType.S)
                )
                .withProvisionedThroughput(new ProvisionedThroughput()
                        .withReadCapacityUnits(2L)
                        .withWriteCapacityUnits(2L)
                )
          );
    }
    
    public static void createDplExtractionTable(AmazonDynamoDB dynamoDB) {
        dynamoDB.createTable(new CreateTableRequest()
                .withTableName(DPL_EXTRACTION_STATUS_TABLE)
                .withKeySchema(
                        new KeySchemaElement().withKeyType(KeyType.HASH).withAttributeName(ExtractionStatusTableColumns.sourceAvailDate.name()),
                        new KeySchemaElement().withKeyType(KeyType.RANGE).withAttributeName(ExtractionStatusTableColumns.tableName.name())
                )
                .withAttributeDefinitions(
                        new AttributeDefinition()
                                .withAttributeName(ExtractionStatusTableColumns.sourceAvailDate.name())
                                .withAttributeType(ScalarAttributeType.S),
                        new AttributeDefinition()
                                .withAttributeName(ExtractionStatusTableColumns.tableName.name())
                                .withAttributeType(ScalarAttributeType.S)
                )
                .withProvisionedThroughput(new ProvisionedThroughput()
                        .withReadCapacityUnits(2L)
                        .withWriteCapacityUnits(2L)
                )
          );
    }

}
