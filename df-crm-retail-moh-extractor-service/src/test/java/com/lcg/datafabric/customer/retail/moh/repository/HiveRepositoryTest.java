package com.lcg.datafabric.customer.retail.moh.repository;

import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.lcg.datafabric.customer.retail.moh.dto.ControlTableResponse;
import com.lcg.datafabric.customer.retail.moh.repository.HiveRepository;

@IfProfileValue(name = "spring.profiles.active", value = "local")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class HiveRepositoryTest {
	
	@Autowired
	private HiveRepository hiveRepo;
	
	@Test
	public void testReadFromControlTable() {
		
		LocalDate lDate = LocalDate.now();
		Optional<ControlTableResponse> optResponse = hiveRepo.queryControlTableForAvailableData(lDate);
		
		assertTrue(optResponse.isPresent());
	}
}
