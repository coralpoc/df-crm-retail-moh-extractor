package com.lcg.datafabric.customer.retail.moh.service;

import com.lcg.common.metadata.EventMetadataImpl;
import com.lcg.customer.cdm.model.CustomerRecord;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerEvent {
    private EventMetadataImpl metadata;
    private CustomerRecord payload;
}
