package com.lcg.datafabric.customer.retail.moh.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ReflectionUtils;
import org.testcontainers.containers.GenericContainer;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.lc.df.logging.DataFabricLogger2;
import com.lcg.customer.cdm.model.CustomerRecord;
import com.lcg.datafabric.customer.retail.moh.RawDBHelperData;
import com.lcg.datafabric.customer.retail.moh.dto.ControlTableResponse;
import com.lcg.datafabric.customer.retail.moh.extraction.status.DplExtractorTableCreator;
import com.lcg.datafabric.customer.retail.moh.extraction.status.DynamoDBTestUtils;
import com.lcg.datafabric.customer.retail.moh.repository.HiveRepository;
import com.lcg.datafabric.customer.retail.moh.service.DynamoDBStatusService;
import com.lcg.datafabric.customer.retail.moh.service.ExtractionService;
import com.lcg.datafabric.customer.retail.moh.service.StreamerService;
import com.lcg.datafabric.customer.retail.moh.view.dynamodb.ExtractionStatusTableColumns;
import com.lcg.datafabric.customer.retail.moh.view.status.ViewStatus;
@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
public class ExtractionServiceIT {
	
	private final static String CUSTOMER_V2_TOPIC = "df-customer-v2";
	
	@Value("kafka.temp-topic")
	private final static String RETAIL_TOPIC = "df-customer-retail";
	
	private final static String RETAIL_VIEW_NAME = "SFMC_RETAIL";
	private final static String DPL_EXTRACTION_STATUS_TABLE = "dpl-extraction-status";
	private final static String RETAIL_EXTRACTION_STATUS_TABLE = "retail-extraction-status";
	
	
	@ClassRule 
	public static KafkaEmbedded embeddedKafka = new KafkaEmbedded( 1, true, 1, new String[] {CUSTOMER_V2_TOPIC,RETAIL_TOPIC});
	
	@ClassRule
	 public static GenericContainer dynamoDb = new GenericContainer("dwmkerr/dynamodb:latest")
	            .withExposedPorts(8000);
	 
	 private static AmazonDynamoDB dynamoDbClient;
	 
	 @Autowired
	 private DynamoDBStatusService service;
	 
	 @Autowired
	 private ExtractionService extractionService;
	 
	 @Mock
	 private HiveRepository hiveRepository;
	 
	 
	 @BeforeClass
	 public static void initializeLocalDynamoDB() {
	      dynamoDbClient = DynamoDBTestUtils.buildLocalDynamoDBClient(
	                						dynamoDb.getContainerIpAddress(),
	                						dynamoDb.getMappedPort(8000));
	      DynamoDBTestUtils.waitForDynamoDbToBeReady(dynamoDbClient, 30000);
	      DplExtractorTableCreator.createRetailExtractionTable(dynamoDbClient);
	      
	 }
	 
	 @AfterClass
	 public static void shutdownLocalDynamoDB() {
	      dynamoDbClient.shutdown();
	 }
	 
	 @TestConfiguration
	 public static class config{
		 
		@Value("${kafka.bootstrapServer}") 
		String kafkaBootstrapServer;
		 
		 
		 @Bean
		 public DataFabricLogger2 dataFabricLogger() {
			 return new DataFabricLogger2("Test_Retail_Service");
		 }
		 
		 @Primary
		 @Bean
		 public DynamoDBStatusService dynamoService() {
			 DynamoDBStatusService dynamoDBStatusService =  new DynamoDBStatusService(dataFabricLogger(), 
					 																  RETAIL_EXTRACTION_STATUS_TABLE,
					 																 RETAIL_VIEW_NAME,
					 																DPL_EXTRACTION_STATUS_TABLE);
			 dynamoDBStatusService.setAmazonDynamoDB(dynamoDbClient, RETAIL_EXTRACTION_STATUS_TABLE);
			 return dynamoDBStatusService;
		 }
		 
		 //@Primary
		 @Bean
		 public StreamerService streamerService() {
			 return new StreamerService(kafkaBootstrapServer);
		 
		 }
		 
		 @Bean
		 public ExtractionService extractionService() {
			 ExtractionService extractionService = new ExtractionService();
			 extractionService.setStreamerService(streamerService());
			 extractionService.setDynamoDBStatusService(dynamoService());
			 extractionService.setDataFabricLogger(dataFabricLogger());
			 
			 
			 return extractionService;
		 }
		 
	 }
	 
	 @Test
	 public void testEndToEndExtractionOperationForStreaming() {
		 
		 extractionService.setHiveRepository(hiveRepository);
		 when(hiveRepository.fetchAllRetailData())
		 	.thenReturn(getAllRetailData());
		 
		 ControlTableResponse controlTableResp = new ControlTableResponse();
		 controlTableResp.setEtlDateTime(LocalDateTime.now());
		 controlTableResp.setRecordCount(3);
		 
		 when(hiveRepository.queryControlTableForAvailableData(LocalDate.now()))
		 	.thenReturn(Optional.of(controlTableResp));
		 
		 extractionService.startExtraction();
		 
		 verifyDynamoDbStatusSet();
		 
		 CustomerKafkaConsumer kafkaConsumer = new CustomerKafkaConsumer(embeddedKafka);
		 List<CustomerRecord> recordList = kafkaConsumer.pullAvailableKafkaRecords(CUSTOMER_V2_TOPIC);
		 
		 verifyComsumedRecordIsCorrect(recordList,3);
	 }
	 
	 
	 @Test
	 public void testEndToEndExtractionOperationForKafka() throws JsonParseException, JsonMappingException, IOException {
		 
		 extractionService.setHiveRepository(hiveRepository);
		 when(hiveRepository.fetchAllRetailData())
		 	.thenReturn(getAllRetailData());
		 extractionService.setStreamerEnabled(false);
		 
		 ControlTableResponse controlTableResp = new ControlTableResponse();
		 controlTableResp.setEtlDateTime(LocalDateTime.now());
		 controlTableResp.setRecordCount(3);
		 
		 when(hiveRepository.queryControlTableForAvailableData(LocalDate.now()))
		 	.thenReturn(Optional.of(controlTableResp));
		 
		 extractionService.startExtraction();
		 
		 verifyDynamoDbStatusSetForKafkaExtracts();
		 
		 CustomerKafkaConsumer kafkaConsumer = new CustomerKafkaConsumer(embeddedKafka);
		 List<Map<String,Object>> recordList = kafkaConsumer.pullAvailableKafkaRecordForMaps(RETAIL_TOPIC);
		 
		 verifyComsumedRecordIsCorrect(recordList,3);
	 }
	 
	 private void verifyComsumedRecordIsCorrect(List recordList,int expectedCount) {
		 
		 assertTrue("Expected comsumed records from topic is wrong", expectedCount == recordList.size() );
	 }
	 
	 private void verifyDynamoDbStatusSet() {
		 
		 Map<String, AttributeValue> itemMap = service.getRetailViewForDate(LocalDate.now());
		 
		 assertTrue(!itemMap.isEmpty());
		 
		 assertEquals(RETAIL_VIEW_NAME,itemMap.get(ExtractionStatusTableColumns.tableName.name()).getS());
		 
		 assertEquals(ViewStatus.SENT_TO_KAFKA.name(),itemMap.get(ExtractionStatusTableColumns.extractionStatus.name()).getS());
		 
		 String sourceAvailDate = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
		 
		 assertEquals(sourceAvailDate,itemMap.get(ExtractionStatusTableColumns.sourceAvailDate.name()).getS());
		 
		 String batchNo = RawDBHelperData.generateBatchNo();
		 
		 assertTrue(itemMap.get(ExtractionStatusTableColumns.batchNumber.name()).getS().startsWith(batchNo));
	 }
	 
	 private void verifyDynamoDbStatusSetForKafkaExtracts() {
		 
		 Map<String, AttributeValue> itemMap = service.getRetailViewForDate(LocalDate.now());
		 
		 assertTrue(!itemMap.isEmpty());
		 
		 assertEquals(RETAIL_VIEW_NAME,itemMap.get(ExtractionStatusTableColumns.tableName.name()).getS());
		 
		 assertEquals(ViewStatus.EXTRACTED.name(),itemMap.get(ExtractionStatusTableColumns.extractionStatus.name()).getS());
		 
		 String sourceAvailDate = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
		 
		 assertEquals(sourceAvailDate,itemMap.get(ExtractionStatusTableColumns.sourceAvailDate.name()).getS());
		 
		 String batchNo = RawDBHelperData.generateBatchNo();
		 
		 assertTrue(itemMap.get(ExtractionStatusTableColumns.batchNumber.name()).getS().startsWith(batchNo));
	 }
	 
	 private List<Map<String,Object>> getAllRetailData(){
		 
		 RawDBHelperData rawDBHelper = new RawDBHelperData();
		 return rawDBHelper.buildMapDataFromFile("data/raw-retail-data-correct.txt");
	 }
		 

}
