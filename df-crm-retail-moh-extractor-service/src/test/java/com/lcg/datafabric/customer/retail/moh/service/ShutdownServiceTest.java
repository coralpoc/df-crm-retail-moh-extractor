package com.lcg.datafabric.customer.retail.moh.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.testcontainers.containers.GenericContainer;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.retail.moh.extraction.status.DplExtractorTableCreator;
import com.lcg.datafabric.customer.retail.moh.extraction.status.DynamoDBTestUtils;
import com.lcg.datafabric.customer.retail.moh.service.BatchNumberHelper;
import com.lcg.datafabric.customer.retail.moh.service.DynamoDBStatusService;
import com.lcg.datafabric.customer.retail.moh.view.dynamodb.ExtractionStatusTableColumns;
import com.lcg.datafabric.customer.retail.moh.view.status.ViewStatus;

import lombok.extern.slf4j.Slf4j;

@Ignore
@RunWith(MockitoJUnitRunner.class)
@Slf4j
public class ShutdownServiceTest {

	private final static String RETAIL_VIEW_NAME = "SFMC_RETAIL";
	private final static String DPL_EXTRACTION_STATUS_TABLE = "dpl-extraction-status";
	private final static String RETAIL_EXTRACTION_STATUS_TABLE = "retail-extraction-status";

	@ClassRule
	public static GenericContainer dynamoDb = new GenericContainer("dwmkerr/dynamodb:latest").withExposedPorts(8000);

	private static AmazonDynamoDB dynamoDbClient;

	private DynamoDBStatusService service;

	@Mock
	private DataFabricLogger2 dataFabricLogger;

	@BeforeClass
	public static void initializeLocalDynamoDB() {
		dynamoDbClient = DynamoDBTestUtils.buildLocalDynamoDBClient(dynamoDb.getContainerIpAddress(),
				dynamoDb.getMappedPort(8000));
	}

	@AfterClass
	public static void shutdownLocalDynamoDB() {
		dynamoDbClient.shutdown();
	}

	@Before
	public void init() {
		try {
			dynamoDbClient.deleteTable(RETAIL_EXTRACTION_STATUS_TABLE);
		} catch (ResourceNotFoundException re) {
			log.info("Cannot delete because Table does not yet exist");
		}
		DplExtractorTableCreator.createRetailExtractionTable(dynamoDbClient);
		DynamoDBTestUtils.waitForDynamoDbToBeReady(dynamoDbClient, 20000);
		service = new DynamoDBStatusService(dataFabricLogger, RETAIL_EXTRACTION_STATUS_TABLE, RETAIL_VIEW_NAME,
				DPL_EXTRACTION_STATUS_TABLE);
		service.setAmazonDynamoDB(dynamoDbClient, RETAIL_EXTRACTION_STATUS_TABLE);
		ReflectionTestUtils.setField(service, "deleteItemRequest",
				new DeleteItemRequest(RETAIL_EXTRACTION_STATUS_TABLE, getKeyToExtract()));
		ReflectionTestUtils.invokeMethod(service, "initDBQuery");
	}
	
	private Map<String, AttributeValue> getKeyToExtract() {
		Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();
		expressionAttributeValues.put(ExtractionStatusTableColumns.sourceAvailDate.name(),
				new AttributeValue().withS(LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE)));
		expressionAttributeValues.put(ExtractionStatusTableColumns.tableName.name(),
				new AttributeValue().withS(RETAIL_VIEW_NAME));
		return expressionAttributeValues;
	}

	@Test
	public void testDynamoDbStatusDuringRetailFlowRun() {
		LocalDateTime localDate = LocalDateTime.now();
		service.creatRetailEntryForDate(localDate, localDate, BatchNumberHelper.getBatchNumber(localDate));
		service.updateRetailRunStatus(LocalDate.now(), ViewStatus.EXTRACTING);
		assertFalse(service.isOkToShutdownService());
	}
}
