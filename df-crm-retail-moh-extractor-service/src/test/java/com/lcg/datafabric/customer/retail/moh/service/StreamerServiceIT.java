package com.lcg.datafabric.customer.retail.moh.service;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.kafka.test.rule.KafkaEmbedded;
import org.springframework.test.annotation.DirtiesContext;

import com.lc.df.logging.DataFabricLogger2;
import com.lcg.customer.cdm.field.StringField;
import com.lcg.customer.cdm.model.CustomerRecord;
import com.lcg.datafabric.customer.retail.moh.service.StreamerService;
import com.lcg.transform.service.CustomerStreamerRecordProcessor;
import org.mockito.junit.MockitoJUnitRunner;
@Ignore
@RunWith(MockitoJUnitRunner.class)
@DirtiesContext
public class StreamerServiceIT {
	
	@Mock
	private DataFabricLogger2 logger;
	
	@InjectMocks
	private StubStreamerservice streamService;
	
	private final static String TEST_TOPIC = "df-customer-v2";
	
	@ClassRule 
	public static KafkaEmbedded embeddedKafka = new KafkaEmbedded( 1, true, 1, TEST_TOPIC);
	
	public StreamerServiceIT() {
	  streamService = new StubStreamerservice(embeddedKafka.getBrokersAsString());
	}
	
	@Test
	public void testSendCustomerRecordToKafka() throws Exception{
		//when(streamService.getConfigProperty()).thenReturn(testConfigProperty());
		 Boolean testRun = false;
		Random random = new Random();
		CustomerRecord customerRecord = new CustomerRecord();
		customerRecord.getPunter().getAccount().setCustomerIdentifier(new StringField(String.valueOf(random.nextInt())));
		
		streamService.sendMessageToKafka(customerRecord);
		
		List<CustomerRecord> sentList = Arrays.asList(customerRecord);
		
		CustomerKafkaConsumer customerConsumer = new CustomerKafkaConsumer(embeddedKafka);
		List<CustomerRecord> recievedList = customerConsumer.pullAvailableKafkaRecords(TEST_TOPIC);
		
		assertCustomerListIsSame(sentList,recievedList);
		
	}
	
	public void assertCustomerListIsSame(List<CustomerRecord> sentList,List<CustomerRecord> recievedList) {
		
		assertEquals(sentList.size(), recievedList.size());
		int size = sentList.size();
		for(int i=0; i < size; i++) {
			assertCustomerRecordEquals(sentList.get(i),recievedList.get(i));
		}
	}
	
	public void assertCustomerRecordEquals(CustomerRecord origRecord,CustomerRecord consumedRecord) {
		
		assertEquals(origRecord.getPunter().getAccount().getCustomerIdentifier(),
				consumedRecord.getPunter().getAccount().getCustomerIdentifier());
	}
	
	public class StubStreamerservice extends StreamerService{
		
		public StubStreamerservice(String kafkaBootstrapServer) {
			super(kafkaBootstrapServer);
			// TODO Auto-generated constructor stub
		}

		public Properties getConfigProperty(String bootStrap) {
			Properties properties = new Properties();
			properties.put(StreamerService.KEY_BOOTSTRAP_SERVER, bootStrap);
			properties.put(CustomerStreamerRecordProcessor.PROPS_IS_MARKETING_FIELD_LOGIC_REQUESTED, false);
			properties.put(CustomerStreamerRecordProcessor.PROPS_KEY_DER_ENRICHMENT_ENABLED, false);
			
			return properties;
		}
		
		public String getTargetTopic() {
			return TEST_TOPIC;
		}
	}
	


}
