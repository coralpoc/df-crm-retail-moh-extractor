#!/bin/sh

# This runs docker images (zookeeper and Kafka)

DOCKER_COMPOSE_SCRIPTS_ROOT=`pwd`/../docker

if [ "${ON_JENKINS}" ]; then
   export PATH=$PATH:$1/bin
   export HOST_IP=`/sbin/ip -o -4 addr list eth0 | awk '{print $4}' | cut -d/ -f1`
else
   export HOST_IP=`ifconfig en0 | grep inet | grep -v inet6 | cut -d' ' -f2`
fi

if [ "${HOST_IP}" ]; then
   echo "HOST_IP: $HOST_IP"
else
   echo "HOST_IP has not been defined."
   echo "Source the appropriate prepare script before running this script, or set HOST_IP to a valid IP address manually."
   exit 1;
fi

# Remove any shutdown kafka docker containers because they may have the wrong IP address and this will stop the
# Kafka producer scripts from working correctly after leaving the network and rejoining again later.
EXISTING_KAFKA_DOCKER_CONATINER_ID=`docker ps -a | grep kafka |  awk '{print $1}'`
if [ "$EXISTING_KAFKA_DOCKER_CONATINER_ID" ]; then
   echo "Removing previous Kafka docker container with id ${EXISTING_KAFKA_DOCKER_CONATINER_ID}"
   docker rm -f ${EXISTING_KAFKA_DOCKER_CONATINER_ID}
fi

EXISTING_ZOOKEEPER_DOCKER_CONATINER_ID=`docker ps -a | grep zook |  awk '{print $1}'`
if [ "$EXISTING_ZOOKEEPER_DOCKER_CONATINER_ID" ]; then
   echo "Removing previous zookeeper docker container with id ${EXISTING_ZOOKEEPER_DOCKER_CONATINER_ID}"
   docker rm -f ${EXISTING_ZOOKEEPER_DOCKER_CONATINER_ID}
fi

if [ "${ON_JENKINS}" ]; then
   COMMAND="docker-compose -f ${DOCKER_COMPOSE_SCRIPTS_ROOT}/docker-compose-shared-services.yml up -d"
   echo "Running on Jenkins $COMMAND"
   eval $COMMAND
   echo "Waiting 10 seconds for kafka / zookeeper to start before running acceptance tests on Jenkins"
   sleep 10
else
   COMMAND="docker-compose -f ${DOCKER_COMPOSE_SCRIPTS_ROOT}/docker-compose-shared-services.yml up"
   echo "Running on local $COMMAND"
   eval $COMMAND
fi

